---
layout: page
permalink: /about/index.html
title: About the Author
modified: 2015-06-08
image:
  feature: about.jpg
  credit: Lingua, by Jim Sanborn (CC-BY)
  creditlink: http://www.flickr.com/photos/bootbearwdc/3155086536/
---

My name is Harlan Lieberman-Berg, and this is the place that I cajole, curse, muse, proselytize, rant, or some combination of the above.  Currently, I'm working for Akamai in their InfoSec group as a security compliance architect.  Previously, I was the founder of AdvertiSin, an advertising company targeted at the adult industry, and before that an engineering and operations manager at [Chitika](http://www.chitika.com).

In the free software world, I'm a [paultag](http://notes.pault.ag) acolyte and Debian Maintainer, helping out with a handful of different packages.  Formerly, I was the head of kernel security for [Gentoo Linux](http://www.gentoo.org) and a member of their security team.  I also contribute, mostly in small ways, to a variety of other F/OSS projects, including [Ansible](http://www.github.com/ansible/ansible), [cberl](http://www.github.com/chitika/cberl), [nanomsg](http://www.nanomsg.org), [triq](http://www.github.com/hlieberman/triq) and [poolboy](http://www.github.com/devinus/poolboy).  I can be found lurking in various IRC channels on Freenode and OFTC.

---

This is a place for me to discuss whatever is on my mind, mainly with a bent towards interesting things that I'm working on or thinking about in the business, technical, or political spheres.  I may occasionally delve into more personal matters, but for the most part, I'm expecting to keep that to a minimum.  This will be the first time I've regularly kept a blog, nevertheless one under a strict, [Iron Blogger](http://boston.iron-blogger.com/pages/home/) enforced schedule, so we will have to see how often the posting schedule is.  It is not unlikely I will end up owing Iron Blogger a lot of money.

---

If you're looking to get in touch with me, I'm on IRC (active or not) around the clock as hlieberman and check [my email](mailto:blog@setec.io) with a probably unhealthy frequency.  If you want to reach me by secure means, my GPG key (```4096R/0x88237A6A53AB1B2E```) can be found on [SKS Keyservers](https://hkps.pool.sks-keyservers.net/pks/lookup?op=get&search=0x88237A6A53AB1B2E) with the fingerprint ```43DF 758C 18C7 545A 3E1A  4F0A 8823 7A6A 53AB 1B2E```.
