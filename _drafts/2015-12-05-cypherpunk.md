---
layout: post
title: I, Cypherpunk
category: articles
---
## Preface

Let me start by thanking Phillip Rogaway for his paper and talk, "The Moral Character of Cryptographic Work".  If you haven't yet read his paper and you work in technology, especially in any security related field, I strongly urge you to [go read it](http://web.cs.ucdavis.edu/~rogaway/papers/moral-fn.pdf).  It in no small part inspired me to write this out publically, and it is an excellent academic analysis of the topic of privacy and morality as it applies to cryptographers.  Go ahead; this will still be here when you return.

This article is in a similar vein [to several](https://blog.setec.io/2015/11/01/ethics.html) [other articles](https://blog.setec.io/2015/11/15/blame.html) [I have](https://blog.setec.io/articles/2016/01/07/software-kill.html) written recently on the topic of the ethical obligations of software engineers.  Unlike the others, though, and unlike Professor Rogaway's excellent paper, this is neither intended to encourage nor enjoin others to come around to my point of view.  I intend this more as a public declaration of my beliefs &mdash; though I would certainly be excited and honored if this convinced others.

## I am a cypherpunk

This declaration is as much aspirational as it is actual, in the same way that I would consider the statement "I write good code" or "I am a nice person" to be.  It is, in my mind, a goal to strive for rather than an metric to be achieved.  (I would not be surprised if people were to argue that I don't meet the definition &mdash; indeed, I also might not be surprised if people also were to argue the same about my code quality or general moral aligement!)

I've considered myself a cypherpunk for some time now, and though I've certainly not been shy to express my political opinions on crypto, privacy, and security, I've been hesitant to make this kind of declaration so bluntly and publicly.  Though many people in the information security industry harbor ideals similar to the cypherpunk movement, to many ears the term is a loaded one.  In an era where the specter of terrorism is invoked to argue for backdoors in encryption<sup>[[1]](http://www.businessinsider.com/fbi-director-terrorism-encryption-garland-2015-12?op=1)</sup>, where the former CIA director accuses Edward Snowden of having blood on his hands for increasing the use of encryption and calling for him to be hanged<sup>[[2]](http://thehill.com/blogs/blog-briefing-room/260817-ex-cia-director-snowden-should-be-hanged-for-paris)</sup>, and when the New York Times issues an article (and retracts it shortly afterwards) implicitly blaming the recent attacks in Paris on encryption<sup>[[3]](http://www.techeye.net/news/new-york-times-pulls-paris-encryption-story)</sup>, this kind of declaration puts me clearly on "a side".  Taking sides in any political issue can be divisive, of course, and can potentially lead to consequences &mdash; social, financial, what have you.

So be it.

## What is a Cypherpunk?

"Great, so you're a Cypherpunk.  I think I know what that is!"

<figure>
<video muted autoplay loop src="/assets/images/cypherpunk.webm">A man in a trenchcoat skateboarding into a room with mainframes that have electricity arcing over them in the background.</video>
<figcaption>This is not a cypherpunk. (<cite>Hackers, &copy; 1995 United Artists</cite>)</figcaption>
</figure>

Not so much.

The cypherpunk community was started in the early '90s by Eric Hughes (author of [A Cypherpunk's Manifesto](https://w2.eff.org/Privacy/Crypto/Crypto_misc/cypherpunk.manifesto)), Timothy May (author of [The Ciphernomicon](http://www.cypherpunks.to/faq/cyphernomicron/cyphernomicon.html)), and John Gilmore (creator of the alt.* Usenet heirarchy, among many other things).  They were active advocates of privacy and cryptography, creating and distributing many technologies whose descendents are still in use today.

Fundamentally, as outlined by [A Cypherpunk's Manifesto](https://w2.eff.org/Privacy/Crypto/Crypto_misc/cypherpunk.manifesto), Cypherpunks believe that privacy &mdash; the power to reveal something to whom, and only whom, you want &mdash; is critical to the functioning of a modern society.  Believing that, we pledge ourselves to protect privacy, for "We cannot expect governments, corporations, or other large, faceless organizations to grant us privacy out of their beneficence.  It is to their advantage to speak of us, and we should expect that they will speak." (Ibid.)  To that end, Cypherpunks dedicate themselves in particular, to write code over other forms of activism &mdash; code that is free for anyone to use, for any purpose, anywhere in the world[^fn1].

## Why?



[^fn1]: When the manifesto was published in 1993, it was illegal for anyone in the United States to export any software with strong cryptography in it without the permission of the government due to its categorization as a munition (along with nuclear weapons, explosives, vessels of war, etc.)  Violations of this provision were punishable by a fine up to one million USD, as well as a maximum of twenty years inprisonment.  <cite>22 CFR §2778c</cite>