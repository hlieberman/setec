---
layout: post
title: The Importance of Community
category: articles
---

While this article is intended to be mostly about free software projects, many of the points apply equally to companies &mdash; especially startups, where the contributor pool is small.

It is not a particularly controversial opinion that maintaining a healthy community is an important part of being a good steward of any kind of project.  This is especially true for projects where the pool of people contributing are doing so without any direct financial incentive, whether potential, like startup options, or immediate, such as being paid.  Some people would even put it high on the list of important attributes of a project, maybe up there with "strong technical leads", "a clear road map for contributors", or "a bug-tracking system".  All good things!  I believe that this doesn't go nearly far enough.  Instead, I believe:

**The single most important thing that a project lead must do in order for their project to succeed is cultivate a strong, healthy community**.

## Really, the _most_ important thing?

At first, I admit, this idea seems a bit far-fetched, if not balls-to-the-wall nuts.  Sure, community is important and all, but is it really more important than having a strong technical lead who can develop useful software?  More important than a way to track bugs?  More important than code that fucking _builds_?!

Yes.

## What crack are you smoking (and where can I get some?)

Alright, alright, hold on.  Before you go on Hacker News and question not only my sanity but whether my parents had a habit of feeding me lead smoothies as a child, let me deconstruct this a bit.

### What does a healthy community mean?

In order for a project to be healthy, it must keep two different groups of people happy and satisfied: the users, and the contributors.

The users of the project &mdash; who may be end-users, or other developers of different projects that depend on yours &mship; want a product that they feel comfortable using.  This means the project must do something useful for them, must be reliable enough for them to use, must be findable, must be responsive to issues and concerns.  If any of these are missing, the project will be in an "unhealthy" state as it relates to its users.

On the other hand, if the contributors to the project aren't happy, they will inevitably destroy the project, either through conflicts that waste resources and cause inaction, or through the contributors burning out and stopping their work on the project.  Though projects with large contributor bases can, to some extent, manage some turnover on the contributor side, the Pareto principle weighs heavily on open source projects.

These two groups are, at least partially, in tension: users will always gravitate towards wanting more functionality, more bug fixes, more support, more documentation, and contributors have a limited pool of time and resources to spend on that ever-growing demand.