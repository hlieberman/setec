---
layout: post
title: Why Text Communications Is Freeing
category: articles
---

My career in tech owes a large debt to the fact that the primary method of communication in the free software world is text.  Just text &mdash; not voice, and definitely not video.  Though I think that I would probably have gotten involved to some extent some day, it wouldn't have been to this extent, and my life would have looked quite a bit different.  I don't think this is an unusual experience; in fact, I think no small part of the diversity (such as it is) in the free software community is because of that.
